import { TestBed } from '@angular/core/testing';

import { AstrometryService } from './astrometry.service';

describe('AstrometryService', () => {
  let service: AstrometryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AstrometryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

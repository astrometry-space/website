import { Component, OnInit, isDevMode } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AstrometryService } from 'src/astrometry.service';
import { delay } from 'rxjs/internal/operators';
import {COMMA, ENTER} from '@angular/cdk/keycodes';

import * as _ from 'lodash';
import { MatChipInputEvent } from '@angular/material/chips';

@Component({
  selector: 'app-submit',
  templateUrl: './submit.component.html',
  styleUrls: ['./submit.component.scss']
})
export class SubmitComponent implements OnInit {

  isDev = isDevMode();

  params: {}

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  submitForm = new FormGroup({
    file_upload: new FormControl(false),
    url: new FormControl('http://nova.astrometry.net/image/6004185'),
    tags: new FormControl(['demo']),
    description: new FormControl('Anonymous upload ' + new Date().toISOString()),
    solve_args: new FormGroup({
      z: new FormControl(2),
      ["crpix-center"]: new FormControl(false),
      parity: new FormControl(false),
      invert: new FormControl(false),
      sextractor: new FormControl(false),
      limits: new FormGroup({
        enabled: new FormControl(false),
        ra: new FormControl(113),
        dec: new FormControl(15),
        radius: new FormControl(12)
      }),
      scale: new FormGroup({
        scale_units: new FormControl('degwidth'),
        scale_low: new FormControl(0.1),
        scale_high: new FormControl(180)
      }),
      tweak_order: new FormControl(2),
      star_positional_error: new FormControl(1)
    })
  })

  addTag(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    // Add our tag
    if ((value || '').trim()) {
      var last_val = this.submitForm.value.tags.slice();
      last_val.push(value.trim());
      this.submitForm.controls['tags'].setValue(last_val)
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeTag(tag: string): void {
    var last_val = this.submitForm.value.tags.slice();
    const index = last_val.indexOf(tag);
    last_val.splice(index,1)
    if (index >= 0) {
      this.submitForm.controls['tags'].setValue(last_val);
    }
  }


  uploadFile: File = null;

  jobs = [];

  // remove this job from the array of submitted jobs:
  acknowledgeJob(j) {
    _.remove(this.jobs, (x) => {
      return x === j;
    })
  }

  getFile(file: File): void {
    this.uploadFile = file;
  }

  makeParams(): void {
    const F = this.submitForm.value;
    const S = F.solve_args;

    // set the solve flags:
    var A: any = {}
    var params: any = {
      solve_args: A
    };

    // downsample:
    A.z = S.z;

    // crpix center:
    if (S["crpix-center"]) {
      A["crpix-center"] = {};
    }

    // parity:
    if (S.parity) {
      if (['pos', 'neg'].indexOf(S.parity) >= 0) {
        A.parity = S.parity;
      }
    }

    // tweak:
    A["tweak-order"] = S.tweak_order;

    // star positional error:
    A["pixel-error"] = S.star_positional_error;

    // invert:
    if ( S.invert ) {
      A["invert"] = {}
    }

    // sextractor:
    if ( S.sextractor ) {
      A["use-sextractor"] = {}
    }

    // scale:
    A["scale-low"] = S.scale.scale_low
    A["scale-high"] = S.scale.scale_high
    A["scale-units"] = S.scale.scale_units

    // limits:
    if ( S.limits.enabled ) {
      A["ra"] = S.limits.ra;
      A["dec"] = S.limits.dec;
      A["radius"] = S.limits.radius;
    }

    params.description = F.description;
    params.tags = F.tags;

    // if it's a url solve, get the URL:
    if ( !F.file_upload ) {
      params.image = F.url;
    }

    this.params = params
  }

  submit(): void {
    const F = this.submitForm.value;
    if (F.file_upload) {
      var submission$ = this.astroSvc.agentSubmitUpload(this.params,this.uploadFile);
    } else {

      var submission$ = this.astroSvc.agentSubmit(this.params)
    }
    submission$.pipe(delay(1000)).subscribe(data => {
      this.jobs.push(data.job_id);
      this.astroSvc.jobList();
    });
  }

  constructor(public astroSvc: AstrometryService) { }

  ngOnInit(): void {
    this.makeParams();
    this.submitForm.valueChanges.subscribe(v => {
      this.makeParams();
    })

  }

}

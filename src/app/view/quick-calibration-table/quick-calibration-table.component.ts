import { Component, OnInit, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-quick-calibration-table',
  templateUrl: './quick-calibration-table.component.html',
  styleUrls: ['./quick-calibration-table.component.scss']
})
export class QuickCalibrationTableComponent implements OnInit, OnDestroy {

  @Input() data;
  kv: any[] = [];
  datasubs: any;

  orientation(): any {
    var orient = this.data.value.calibration.orientation;
    if (['image/jpeg', 'image/png', 'image/gif'].indexOf(this.data.value.original_file.mime) >= 0) {
      return (((180 - orient) + 360) % 360)
    } else {
      return orient
    }

  }

  constructor() { }

  ngOnInit(): void {

    this.datasubs = this.data.subscribe(data => {

      if (data?.calibration) {
        this.kv = []
        Object.keys(data.calibration).forEach(k => {
          this.kv.push({
            key: k,
            value: data.calibration[k]
          })
        })

      }

    })

  }

  ngOnDestroy(): void {
    this.datasubs.unsubscribe();
  }

}

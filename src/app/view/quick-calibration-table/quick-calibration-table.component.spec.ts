import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuickCalibrationTableComponent } from './quick-calibration-table.component';

describe('QuickCalibrationTableComponent', () => {
  let component: QuickCalibrationTableComponent;
  let fixture: ComponentFixture<QuickCalibrationTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuickCalibrationTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickCalibrationTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

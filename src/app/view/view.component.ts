import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AstrometryService } from 'src/astrometry.service';
import { interval, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit, OnDestroy {
  cacheBuster: Number = Date.now();
  jobID: string;
  data = new BehaviorSubject(null);
  subs: any[] = [];

  constructor(private route: ActivatedRoute, public astroSvc: AstrometryService) { }

  ngOnInit(): void {

    this.subs.push(this.route.paramMap.subscribe(paramMap => {
      this.jobID = paramMap.get('id');

      // initial info and logs retreival: 
      if (this.jobID) {
        this.subs.push(this.astroSvc.jobInfo(this.jobID).subscribe(data => {
          this.data.next(data);
        }));
      }

      // then initiate polling:
      var poll = interval(3000).subscribe(count => {

        // check for calibration results, if we don't have them:
        // TODO: skip this if the job has failed
        if (!this.data.value?.calibration) {
          console.log('getting calibration...')
          this.subs.push(this.astroSvc.jobInfo(this.jobID).subscribe(data => {
            this.data.next(data);
            if (this.data.value?.calibration) {
              // once (if) the calibration shows up, force reload the images:
              this.cacheBuster = Date.now();
            }
          }));
        }

      });

      // add the poll subscribtion to our list of subscriptions to make sure
      // it's destroyed when we leave the page:
      this.subs.push(poll);

    }));

  }

  ngOnDestroy() {
    // unsubscribe from everything:
    this.subs.forEach(S => {
      S.unsubscribe();
    })
  }

}

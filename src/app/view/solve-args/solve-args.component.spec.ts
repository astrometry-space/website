import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolveArgsComponent } from './solve-args.component';

describe('SolveArgsComponent', () => {
  let component: SolveArgsComponent;
  let fixture: ComponentFixture<SolveArgsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolveArgsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolveArgsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

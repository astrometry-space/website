import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-solve-args',
  templateUrl: './solve-args.component.html',
  styleUrls: ['./solve-args.component.scss']
})
export class SolveArgsComponent implements OnInit {

  @Input() data:any;

  constructor() { }

  ngOnInit(): void {

  }

}

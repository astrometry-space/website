import { Component, OnInit, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-calibration-table',
  templateUrl: './calibration-table.component.html',
  styleUrls: ['./calibration-table.component.scss']
})
export class CalibrationTableComponent implements OnInit, OnDestroy {

  @Input() data;

  kv: any[] = [];

  datasubs: any;

  constructor() { }

  ngOnInit(): void {

    this.datasubs = this.data.subscribe(data => {

      if (data?.calibration) {
        this.kv = []
        Object.keys(data.calibration).forEach(k => {
          this.kv.push({
            key: k,
            value: data.calibration[k]
          })
        })

      }

    });

  }

  ngOnDestroy(): void {
    this.datasubs.unsubscribe()
  }

}

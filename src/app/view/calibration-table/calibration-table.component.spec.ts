import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalibrationTableComponent } from './calibration-table.component';

describe('CalibrationTableComponent', () => {
  let component: CalibrationTableComponent;
  let fixture: ComponentFixture<CalibrationTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalibrationTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalibrationTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input, OnDestroy, ViewChild, ElementRef, AfterViewInit, DoCheck } from '@angular/core';
import { AstrometryService } from 'src/astrometry.service';
import { interval, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss']
})
export class LogsComponent implements OnInit, OnDestroy, AfterViewInit, DoCheck {

  @Input() jobId;

  logs = new BehaviorSubject<string[]>(['no logs yet!']);
  live_logs: boolean = true;
  since:Number = 0;
  pollsubs: any;

  @ViewChild('scrollframe', {static: false}) scrollFrame: ElementRef;
  private scrollContainer: any;
  isNearBottom: boolean = true; // is the user scrolled to the bottom of the logs?

  // determine if user is near bottom of pre block:
  isUserNearBottom(): boolean {
    const threshold = 15;
    const position = this.scrollContainer.scrollTop + this.scrollContainer.offsetHeight;
    const height = this.scrollContainer.scrollHeight;
    return position >= height - threshold;
  }

  // scroll to bottom of pre block:
  scrollToBottom(): void {
    this.scrollContainer.scroll({
      top: this.scrollContainer.scrollHeight,
      left: 0,
      behavior: 'smooth'
    });
  }

  // called by scroll event in pre block:
  scrolled(event: any): void {
    this.isNearBottom = this.isUserNearBottom();
  }

  // update logs, depending on whether they're from job agent, or S3:
  updateLogs(logs:string,stored=false): void {
    if ( stored ) {
      // if these are the store logs from S3, replace the logs:
      this.logs.next([logs])
    } else {
      // otherwise assume they should be appended:
      if ( logs ) {
        this.logs.next(this.logs.value.concat(logs.split('\n')))
      }
    }
  }

  constructor(private astroSvc: AstrometryService) { }

  ngOnInit(): void {

    // poll agent every 3 seconds for logs:
    this.pollsubs  = interval(3000).subscribe( count => {
      this.astroSvc.agentLogs(this.jobId, this.since).subscribe(
        (data) => {
          this.updateLogs(data.logs);
          this.since = data.time;
        },
        (err) => {
          // console.log('Retrieving live logs failed: trying stored logs');
          this.astroSvc.jobLogs(this.jobId).subscribe( logs => {
            this.updateLogs(logs,true);
            this.pollsubs.unsubscribe()
            this.live_logs = false;
            // console.log('Retrieved stored logs!');
          }, (err) => {
            // console.log('Retrieving stored logs failed... trying all again');
          });
        });

    })

  }

  ngAfterViewInit() {
    this.scrollContainer = this.scrollFrame.nativeElement;
  }

  ngDoCheck(): void {
    // scroll to the bottom if the user was tailing logs:
    if (this.scrollContainer && this.isNearBottom) {
      this.scrollToBottom();
    }
  }

  ngOnDestroy(): void {
    this.pollsubs.unsubscribe();
  }

}

import { Component, OnInit, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-events-table',
  templateUrl: './events-table.component.html',
  styleUrls: ['./events-table.component.scss']
})
export class EventsTableComponent implements OnInit, OnDestroy {

  @Input() data;
  events = null
  datasubs: any;

  constructor() { }

  ngOnInit(): void {

    this.datasubs = this.data.subscribe(data => {

      // hack to get sorted event times: this should really be fixed in the backend!
      if (data?.time) {
        var events = [
          {
            stage: 'birth',
            event: 'submitted',
            time: data.time.submitted
          }
        ]

        Object.keys(data.time).forEach(stage => {
          if (stage != 'submitted') {
            Object.keys(data.time[stage]).forEach(event => {
              events.push({
                stage: stage,
                event: event,
                time: data.time[stage][event]
              })
            })
          }
        })

        events.sort(function compare(a, b) {
          return a.time - b.time;
        });

        this.events = events;
      }

    })

  }

  ngOnDestroy(): void {
    this.datasubs.unsubscribe();
  }

}

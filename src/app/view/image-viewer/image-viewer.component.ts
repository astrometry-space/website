import { Component, OnInit, Input } from '@angular/core';
import { AstrometryService } from 'src/astrometry.service';

@Component({
  selector: 'app-image-viewer',
  templateUrl: './image-viewer.component.html',
  styleUrls: ['./image-viewer.component.scss']
})
export class ImageViewerComponent implements OnInit {

  // these will be updated via the view component:
  @Input() jobID: string;
  @Input() cacheBuster: number;

  constructor(public astroSvc: AstrometryService) { }

  ngOnInit(): void {
  }

}

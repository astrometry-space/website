import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobTableComponent } from './job-table/job-table.component';
import { SubmitComponent } from './submit/submit.component';
import { ViewComponent } from './view/view.component';


const routes: Routes = [
  { path: 'search', component: JobTableComponent },
  { path: 'submit', component: SubmitComponent },
  { path: 'view/:id', component: ViewComponent },
  { path: '',   redirectTo: '/search', pathMatch: 'full' },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

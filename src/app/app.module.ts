import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JobTableComponent } from './job-table/job-table.component';
import { SubmitComponent } from './submit/submit.component';
import { ViewComponent } from './view/view.component';

import { ImagePreloadDirective } from './image-preload.directive';

import { NgxJsonViewerModule } from 'ngx-json-viewer';


import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatRadioModule} from '@angular/material/radio';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTabsModule} from '@angular/material/tabs';
import {MatSelectModule} from '@angular/material/select';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {TextFieldModule} from '@angular/cdk/text-field';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';

import { CalibrationTableComponent } from './view/calibration-table/calibration-table.component';
import { EventsTableComponent } from './view/events-table/events-table.component';
import { QuickCalibrationTableComponent } from './view/quick-calibration-table/quick-calibration-table.component';
import { ImageViewerComponent } from './view/image-viewer/image-viewer.component';
import { SolveArgsComponent } from './view/solve-args/solve-args.component';
import { LogsComponent } from './view/logs/logs.component';
import { TagsComponent } from './view/tags/tags.component';


@NgModule({
  declarations: [
    AppComponent,
    JobTableComponent,
    SubmitComponent,
    ImagePreloadDirective,
    ViewComponent,
    CalibrationTableComponent,
    EventsTableComponent,
    QuickCalibrationTableComponent,
    ImageViewerComponent,
    SolveArgsComponent,
    LogsComponent,
    TagsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule, ReactiveFormsModule,
    MatTableModule, MatFormFieldModule, MatInputModule, MatButtonModule, 
    MatSlideToggleModule, MatButtonToggleModule, MatRadioModule, MatToolbarModule,
    MatTabsModule, MatSelectModule,MatProgressBarModule, TextFieldModule, MatChipsModule, MatIconModule,
    NgxJsonViewerModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit, HostListener } from '@angular/core';
import { AstrometryService } from 'src/astrometry.service';

@Component({
  selector: 'app-job-table',
  templateUrl: './job-table.component.html',
  styleUrls: ['./job-table.component.scss']
})
export class JobTableComponent implements OnInit {

  constructor(public astroSvc: AstrometryService) { }

  displayedColumns: string[] = ['created', 'id', 'original', 'index'];

  dataSource = this.astroSvc.job_list.value;

  // set visible columns based on window width:
  @HostListener('window:resize', ['$event'])
  dynamicColumns(event=null): void {
    
    if ( event ) {
      var w = Number(event.target.innerWidth);
    } else {
      var w = Number(window.screen.width);
    }
    
    if (w < 800) {
      this.displayedColumns = ['id', 'original'];
    } else {
      this.displayedColumns = ['created', 'id', 'original', 'index'];
    }
  }

  ngOnInit(): void {
    this.dynamicColumns();
  }

}

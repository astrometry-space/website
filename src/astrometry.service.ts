import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { environment } from './environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AstrometryService {


  host: string = environment.api;

  job_list = new BehaviorSubject([]);

  /**
   * submit image url
   * @param params 
   */
  agentSubmit(params): any {
    const url = this.host + '/agent/submit';
    return this.http.post<any>(url,params)
  }
  
  /**
   * submit file upload
   * @param params 
   * @param image 
   */
  agentSubmitUpload(params:any={},image:File): any {
    const url = this.host + '/agent/submit/upload';
    const formData = new FormData();
    formData.append('image',image);
    formData.append('solve_args',JSON.stringify(params?.solve_args));
    formData.append('description',params.description);
    formData.append('tags',JSON.stringify(params.tags));
    return this.http.post<any>(url,formData)
  }

  /**
   * return logs of a running job (before storage)
   * @param jobId 
   * @param since 
   */
  agentLogs(jobId,since:Number=0,mock=false): any {
    var url = this.host + '/agent/logs/' + jobId + "?since=" + since;
    if ( mock ) {
      url = url + "&mock=1";
    }
    return this.http.get<any>(url);
  }

  /**
   * return the list of job ids
   */
  jobList(): void {
    const url = this.host + '/job/list';
    this.http.get<any>(url).subscribe(data => {
      this.job_list.next(data);
    })
  }
  
  jobInfo(jobId:string): any {
    const url = this.host + '/job/' + jobId + '/info';
    return this.http.get<any>(url)
  }

  jobLogs(jobId:string): any {
    const url = this.host + '/job/' + jobId + '/logs';
    return this.http.get(url,{responseType: 'text'})
  }

  /**
   * return URL to original upload image
   * @param jobId 
   */
  image(jobId,image='original'): string {
    return this.host + '/job/' + jobId + '/image/' + image;
  }

  constructor(private http: HttpClient) {
    this.jobList()
  }
}
